// Lab - Standard Template Library - Part 3 - Queues
// Thomas, Felton

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> transactions;

	bool done = false;
	while (!done) {
		cout << "1. Enqueue Transaction"
			<< "2. Continue";

		int choice;
		cin >> choice;

		if (choice == 1) { // Enqueue Transaction
			cout << "Enter amount (positive or negative) for next transaction: ";
			float amount = 0.00;
			cin >> amount;
			transactions.push(amount);
		}
		else if (choice == 2) { // Continue
			float balance = 0.00;
			for (int i = 0; i < transactions.size(); i++) {
				float balance = 0.00;
				cout << transactions.front() << "pushed to account" << endl;
				balance += transactions.front();
				transactions.pop();
			}

			done = true;
		}
	}

    cin.ignore();
    cin.get();
    return 0;
}
