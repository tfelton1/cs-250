// Lab - Standard Template Library - Part 4 - Stacks
// Thomas, Felton

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
    stack<string> letters;

    bool done = false;
    while (!done) {
        cout << "Enter the next letter of the word , or UNDO to undo , or DONE to stop .";

        string choice;
        cin >> choice;

        if (choice == "UNDO") { // Enqueue Transaction
            cout << "Removed " << letters.top() << endl;
            letters.pop();
        }
        else if (choice == "DONE") { // Continue
            done = true;
        }
        else { // Continue
            letters.push(choice);
        }
    }

    cout << "Finished word: ";
    while ( !letters.empty() ) {
        cout << letters.top();
        letters.pop();
    }

    cin.ignore();
    cin.get();
    return 0;
}
