// Lab - Standard Template Library - Part 5 - Maps
// Thomas, Felton

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
    map<char, string> colors;

    colors[ 'r' ] = "FF0000";
    colors[ 'g' ] = "00FF00";
    colors[ 'b' ] = "0000FF";
    colors[ 'c' ] = "00FFFF";
    colors[ 'm' ] = "FF00FF";
    colors[ 'y' ] = "FFFF00";

    cout << "Enter a color letter, or ’q’ to stop: ";

    char color;
    cin >> color;

    while (color != 'q') {
        cout << "Hex: " << colors[ color ] << endl;
        cout << "Enter a color letter, or ’q ’ to stop: ";
        cin >> color;
    }

    cout << "Goodbye" << endl;

    cin.ignore();
    cin.get();
    return 0;
}
