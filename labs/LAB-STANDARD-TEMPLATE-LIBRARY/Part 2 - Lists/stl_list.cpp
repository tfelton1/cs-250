// Lab - Standard Template Library - Part 2 - Lists
// Thomas, Felton

#include <iostream>
#include <string>
#include <list>
using namespace std;

void DisplayList(list <string>& states);

int main()
{
	list<string> states;

	bool done = false;
	while (!done) {
		cout << endl << "MAIN MENU" << endl;
		cout << "1. Add new states to front of list"
			<< "2. Add new state to back of list"
			<< "3. Pop state from front of list"
			<< "4. Pop state from end of list"
			<< "5. Continue";

		int choice;
		cin >> choice;

		if (choice == 1) { // Add new states to front of list
			string newState;
			cout << "What is the state name? ";
			cin >> newState;
			states.push_front(newState);
		}
		else if (choice == 2) { // Add new state to back of list
			string newState;
			cout << "What is the state name? ";
			cin >> newState;
			states.push_back(newState);
		}
		else if (choice == 3) { // Pop state from front of list
			states.pop_front();
		}
		else if (choice == 4) { // Pop state from end of list
			states.pop_back();
		}
		else if (choice == 5) { // Continue
			cout << "ORIGINAL LIST:" << endl;
			DisplayList(states);

			cout << "REVERSED LIST:" << endl;
			states.reverse();
			DisplayList(states);

			cout << "ORIGINAL LIST:" << endl;
			states.sort();
			DisplayList(states);

			cout << "ORIGINAL LIST:" << endl;
			states.reverse();
			DisplayList(states);

			done = true;
		}

	}
    cin.ignore();
    cin.get();
    return 0;
}


void DisplayList ( list <string>& states )
{
	for ( list <string>::iterator it = states.begin();
		it != states.end();
		it ++ )
	{
		cout << *it << "\t";
	}
	cout << endl;
}
