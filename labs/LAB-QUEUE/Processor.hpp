#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
    ofstream output( logFile );
    output << "First Come First Serve (FCFS)" << endl;

    int cycles = 0;
    output << "Processing Job #" << cycles << endl;
    while (jobQueue.Size() != 0)
    {
        output << "CYCLE   " << cycles << "   REMAINING:     " << jobQueue.Front()->fcfs_timeRemaining << endl;
        jobQueue.Front()->Work( FCFS );

        if (jobQueue.Front()->fcfs_done == true)
        {
            jobQueue.Front()->SetFinishTime( cycles, FCFS );
            jobQueue.Pop();
        }
        cycles++;
    }
    output << "Done" << endl;

    output << "First come , first serve results :" << endl << endl;
    output << "Job ID     TIME TO COMPLETE" << endl;
    int i;
    for (i = 0; i < allJobs.size(); i++)
    {
        output << i << "     " << allJobs[i].fcfs_finishTime << endl;
    }
    output << endl << "Total time : ............. " << allJobs[i-1].fcfs_finishTime << endl;
    output << "( Time for all jobs to complete processing )" << endl << endl;

    output << endl << "Average time : ........... " << allJobs[i-1].fcfs_finishTime/(i-1) << endl;
    output << "( The average time to complete , including the wait time while items are ahead of it in the queue .)" << endl;

    output.close();
}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
    ofstream output( logFile );
    output << "Round Robin (RR)" << endl;
    output << "Processing Job #" << jobQueue.Front()->id << "..." << endl;

    int cycles = 0, timer = 0;
    while (jobQueue.Size() != 0)
    {
        if (timer == timePerProcess)
        {
            jobQueue.Front()->rr_timesInterrupted += 1;
            jobQueue.Push(jobQueue.Front());
            jobQueue.Pop();
            timer = 0;
            output << "Processing Job #" << jobQueue.Front()->id << "..." << endl;
        }

        output << "   CYCLE   " << cycles << "   REMAINING:   " << jobQueue.Front()->rr_timeRemaining << endl;

        jobQueue.Front()->Work( RR );

        if (jobQueue.Front()->rr_done == true)
        {
            jobQueue.Front()->SetFinishTime( cycles, RR );
            jobQueue.Pop();
        }

        cycles++, timer++;
    }

    output << "Done" << endl;

    output << "Round Robin results :" << endl << endl;
    output << "Job ID     TIME TO COMPLETE     TIMES INTERRUPTED" << endl;
    int i;
    for (i = 0; i < allJobs.size(); i++)
    {
        output << i << "     " << allJobs[i].rr_finishTime << endl;
    }
    output << endl << "Total time : ............. " << allJobs[i-1].fcfs_finishTime << endl;
    output << "( Time for all jobs to complete processing )" << endl << endl;

    output << endl << "Average time : ........... " << allJobs[i-1].fcfs_finishTime/(i-1) << endl;
    output << "( The average time to complete , including the wait time while items are ahead of it in the queue .)" << endl;

    output << endl << "Round Robin Interval: ........... " << timePerProcess << endl;
    output << "( Every n units of time , move the current item being processed to the back of the queue and start working on the next item )" << endl;

    output.close();
}

#endif
