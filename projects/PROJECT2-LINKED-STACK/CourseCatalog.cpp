#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "/home/thomas/Downloads/cs-250/projects/PROJECT2-LINKED-STACK/EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp" // LinkedStack

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "/home/thomas/Downloads/cs-250/projects/PROJECT2-LINKED-STACK/courses.txt" );
	// ifstream input("courses.txt");

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses()
{
    Menu::Header( "VIEW COURSES" );

    cout << "#" << "   " << "CODE" << "   " << "TITLE" << endl;
    cout << "      PREREQS" << endl;
    Menu::DrawHorizontalBar(45);

    for(int i = 0; i < m_courses.Size(); i++)
    {
        cout << i << "   " << m_courses[i].code << "   " << m_courses[i].name << endl;
        cout << "      " << m_courses[i].prereq << endl;
    }
}

Course CourseCatalog::FindCourse( const string& code )
{
    for(int i = 0; i < m_courses.Size(); i++)
    {
        if (m_courses[i].code == code)
        {
            return m_courses[i];
        }
    }
    throw CourseNotFound( "Error!" );
}

void CourseCatalog::ViewPrereqs()
{
    cout << endl << "Enter course code" << endl;
    string courseCode;
    cin >> courseCode;
    Course current;
    try
    {
        current = FindCourse(courseCode);
    }
    catch ( exception& error )
    {
        cout << error.what() << endl;
        cout << "Error ! Unable to find course " << courseCode << endl;
        return;
    }
    
    Menu::Header( "GET PREREQS" );

    LinkedStack<Course> prereqs;
    prereqs.Push(current);

    while ( current.prereq != "" )
    {
        try
        {
            prereqs.Push(FindCourse(current.prereq));
        }
        catch ( exception& error )
        {
            cout << error.what() << endl;
            return;
        }
        current.Clear();
        current = prereqs.Top();
    }

    int i = 0;
    while ( prereqs.Size() != 0 )
    {
        i++;
        cout << i << ".   " << prereqs.Top().code << "  " << prereqs.Top().name << endl;
        prereqs.Pop();
    }
}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
