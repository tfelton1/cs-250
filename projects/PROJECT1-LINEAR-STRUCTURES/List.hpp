#ifndef _LIST_HPP
#define _LIST_HPP

#include<iostream>
using namespace std;

const int ARRAY_SIZE = 100;

template <typename T>
class List
{
private:
    // private member variables
    int m_itemCount;
	T m_arr[ARRAY_SIZE];

    // functions for interal-workings
    bool ShiftRight( int atIndex )
    {
        // check if array is full before shifting right
        if (IsFull()) {
            cout << "m_arr is full. Cannot shift right." << endl;
            return false;
        }
        // check if atIndex is valid
        if ( atIndex < 0 || atIndex > m_itemCount) {
            cout << "atIndex is invalid. Provide number greater than 0 and less than m_itemCount." << endl;
            return false;
        }

        // Shift values from right to left to avoid overwriting
        for (int i = m_itemCount; atIndex < i; i--)
        {
            m_arr[i] = m_arr[i-1];
        }

        return true;
    }

    bool ShiftLeft( int atIndex )
    {
        // check if array is empty before shifting left
        if (IsEmpty()) {
            cout << "m_arr is empty. No operation was performed." << endl;
            return false;
        }
        // check if atIndex is valid
        if ( atIndex < 0 || atIndex > m_itemCount) {
            cout << "atIndex is invalid. Provide number greater than 0 and less than m_itemCount." << endl;
            return false;
        }

        // Shift values from left to right to avoid overwriting
        for (int i = atIndex; i < m_itemCount; i++)
        {
            m_arr[i] = m_arr[i+1];
        }

        return true;
    }

public:
    List()    :   m_itemCount(0)
    {
    }

    ~List()
    {
    }

    // Core functionality
    int     Size() const
    {
        return m_itemCount;
    }

    bool    IsEmpty() const
    {
        return (m_itemCount == 0);
    }

    bool    IsFull() const
    {
        return (m_itemCount == ARRAY_SIZE);
    }

    bool    PushFront( const T& newItem )
    {
        // Check if list is full
        if (IsFull()) {
            cout << "List is full!" << endl;
            return false;
        }

        // Shift right at zeroeth index
        if ( !(*this).IsEmpty() ) {
            (*this).ShiftRight(0);
        }
        // Assign zeroeth position with new item
        m_arr[0] = newItem;
        // Increment itemCount value to account for addition
        m_itemCount++;

        return true;
    }

    bool    PushBack( const T& newItem )
    {
        // Check if list is full
        if (IsFull()) {
            cout << "List is full!" << endl;
            return false;
        }

        // Assign last position with new item
        m_arr[m_itemCount] = newItem;
        // Increment itemCount value to account for addition
        m_itemCount++;

        return true;
    }

    bool    Insert( int atIndex, const T& item )
    {
        if (IsFull()) {
            cout << "List is full!" << endl;
            return false;
        }

        // Shift right at atIndex
        ShiftRight(atIndex);
        // Assign zeroeth position with new item
        m_arr[atIndex] = item;
        // Increment itemCount value to account for addition
        m_itemCount++;

        return true;
    }

    bool    PopFront()
    {
        if (IsEmpty()) {
            cout << "m_arr is empty. No operation was performed." << endl;
            return false;
        }

        // First item is overwritten by shifting all items left of zeroeth index to the left.
        ShiftLeft(0);
        // Update itemCount
        m_itemCount--;

        return true;
    }

    bool    PopBack()
    {
        if (IsEmpty()) {
            cout << "m_arr is empty. No operation was performed." << endl;
            return true;
        }

        /*
        No deleting or overwriting is done in this step, but I deallocate the last position
        as a position currently holding a value. In this way, if a new item were added
        directly after this PopBack function is performed, it would overwrite the item
        deallocated in this function. This is called lazy deletion.
        */

        // Update itemCount
        m_itemCount--;

        return true;
    }

    bool    RemoveItem( const T& item )
    {
        if (IsEmpty()) {
            cout << "m_arr is empty. No operation was performed." << endl;
            return false;
        }

        // ShiftLeft at first position in m_arr that matches item. This will overwrite position with item.
        for (int i = 0; i < m_itemCount; i++) {
            if (m_arr[i] == item) {
                ShiftLeft(i);

                // Update itemCount
                m_itemCount--;
            }
        }

        return true;
    }

    bool    RemoveIndex( int atIndex )
    {
        if (IsEmpty()) {
            cout << "m_arr is empty. No operation was performed." << endl;
            return false;
        }

        // ShiftLeft at atIndex.
        ShiftLeft(atIndex);

        return true;
    }

    void    Clear()
    {
        m_itemCount = 0;
        return;
    }

    // Accessors
    T*      Get( int atIndex )
    {
        return &m_arr[atIndex];
    }

    T*      GetFront()
    {
        return *m_arr[0];
    }

    T*      GetBack()
    {
        return *m_arr[m_itemCount];
    }

    // Additional functionality
    int     GetCountOf( const T& item ) const
    {
        int CountOf = 0;
        for (int i = 0; i < m_itemCount; i++) {
            if (m_arr[i] == item) {
                CountOf++;
            }
        }

        return CountOf;
    }

    bool    Contains( const T& item ) const
    {
        for (int i = 0; i < m_itemCount; i++) {
            if (m_arr[i] == item) {
                return true;
            }
        }
        return false;
    }

    friend class Tester;
};


#endif
