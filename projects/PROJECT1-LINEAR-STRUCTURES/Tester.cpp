#include "Tester.hpp"

void Tester::RunTests()
{
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_Remove();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Tester::Test_Init()
{
    DrawLine();
    cout << "TEST: Test_Init" << endl;

    // Put tests here
}

void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;

    // Put tests here
    {       // Test 1
        cout << endl << "Test 1" << endl;
        List<int> testList;

        bool expectedValue = false;
        bool actualValue = testList.ShiftRight(5);

        cout << "Created empty list; ShiftRight(5) should be false." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }

    {       // Test 2
        cout << endl << "Test 2" << endl;
        cout << "Prerequisites: PushBack()" << endl;
        List<int> testList;
        testList.PushBack(5);
        testList.PushBack(4);
        testList.PushBack(3);
        testList.PushBack(2);
        testList.PushBack(1);

        bool expectedValue = true;
        bool actualValue = testList.ShiftRight(2);

        cout << "Created list with 5,4,3,2,1; ShiftRight(2) should be true." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }

    {       // Test 3
        cout << endl << "Test 3" << endl;
        cout << "Prerequisites: PushBack()" << endl;
        List<int> testList;
        testList.PushBack(5);
        testList.PushBack(4);
        testList.PushBack(3);
        testList.PushBack(2);
        testList.PushBack(1);

        bool expectedValue = false;
        bool actualValue = testList.ShiftRight(21);

        cout << "Created list with 5,4,3,2,1; ShiftRight(21) should be false." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }
}

void Tester::Test_ShiftLeft()
{
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl;

    // Put tests here
    {       // Test 1
        cout << endl << "Test 1" << endl;
        List<int> testList;

        bool expectedValue = false;
        bool actualValue = testList.ShiftLeft(5);

        cout << "Created empty list; ShiftLeft(5) should be false." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }

    {       // Test 2
        cout << endl << "Test 2" << endl;
        cout << "Prerequisites: PushBack()" << endl;
        List<int> testList;
        testList.PushBack(5);
        testList.PushBack(4);
        testList.PushBack(3);
        testList.PushBack(2);
        testList.PushBack(1);

        bool expectedValue = true;
        bool actualValue = testList.ShiftLeft(2);

        cout << "Created list with 5,4,3,2,1; ShiftLeft(2) should be true." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }

    {       // Test 3
        cout << endl << "Test 3" << endl;
        cout << "Prerequisites: PushBack()" << endl;
        List<int> testList;
        testList.PushBack(5);
        testList.PushBack(4);
        testList.PushBack(3);
        testList.PushBack(2);
        testList.PushBack(1);

        bool expectedValue = false;
        bool actualValue = testList.ShiftLeft(21);

        cout << "Created list with 5,4,3,2,1; ShiftLeft(21) should be false." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }
}

void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    {       // Test 1
        cout << endl << "Test 1" << endl;
        List<int> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "Create a new list, Size() should be 0." << endl;
        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
        testList.Clear();
    }   // Test end

    {       // Test 2
        cout << endl << "Test 2" << endl;
        List<int> testList;

        testList.PushBack(1);
        testList.PushBack(4);

        int expectedSize = 2;
        int actualSize = testList.Size();

        cout << "Created list, added 2 items, Size() should be 2" << endl;
        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
        testList.Clear();
    }   // Test end

    {       // Test 3
        cout << endl << "Test 3" << endl;
        List<int> testList;

        for (int i = 0; i < 101; i++) {
            testList.PushBack(1);
        }

        int expectedSize = 100;
        int actualSize = testList.Size();

        cout << "Created list, attempted to add 101 items, Size() should be 100" << endl;
        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
        testList.Clear();
    }   // Test end

    {       // Test 4
        cout << endl << "Test 4" << endl;
        List<int> testList;

        testList.PushBack(1);
        testList.PushBack(4);
        testList.PushBack(27);
        testList.PushBack(16);
        testList.PushBack(18);
        testList.PopBack();
        testList.PopFront();

        int expectedSize = 3;
        int actualSize = testList.Size();

        cout << "Created list, added 5 items and deleted 2 items, Size() should be 3" << endl;
        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
        testList.Clear();
    }   // Test end
}

void Tester::Test_IsEmpty()
{
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl;

    // Put tests here
    {     // Test 1
        cout << endl << "Test 1" << endl;
        List<int> testList;

        bool expectedValue = true;
        bool actualValue = testList.IsEmpty();

        cout << "Created empty list; IsEmpty() should be true." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
        testList.Clear();
    }

    {     // Test 2
        cout << endl << "Test 2" << endl;
        cout << "Prerequisites: PushBack()" << endl;
        List<int> testList;

        testList.PushBack(1);

        bool expectedValue = false;
        bool actualValue = testList.IsEmpty();

        cout << "Created list, added 1 item, IsEmpty() should be false." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
        testList.Clear();
    }

    {     // Test 3
        cout << endl << "Test 3" << endl;
        cout << "Prerequisites: PushBack()" << endl;
        List<int> testList;

        for (int i = 0; i < 100; i++) {
            testList.PushBack(1);
        }

        bool expectedValue = false;
        bool actualValue = testList.IsEmpty();

        cout << "Created list, attemped to add 101 items, IsEmpty() should be false." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
        testList.Clear();
    }

    {     // Test 4
        cout << endl << "Test 4" << endl;
        cout << "Prerequisites: PushBack()" << endl;
        List<int> testList;

        for (int i = 0; i < 100; i++) {
            testList.PushBack(1);
        }

        bool expectedValue = false;
        bool actualValue = testList.IsEmpty();

        cout << "Created list, added 100 items, IsEmpty() should be false." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
        testList.Clear();
    }

    {     // Test 5
        cout << endl << "Test 5" << endl;
        cout << "Prerequisites: PushBack()" << endl;
        List<int> testList;

        testList.PushBack(1);
        testList.PushBack(4);
        testList.PushBack(12);
        testList.PushBack(21);
        testList.PushBack(14);
        testList.PopBack();
        testList.PopFront();
        testList.PopBack();
        testList.PopFront();
        testList.PopBack();

        bool expectedValue = true;
        bool actualValue = testList.IsEmpty();

        cout << "Created list, added 5 items and popped 5 items, IsEmpty() should be false." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
        testList.Clear();
    }
}

void Tester::Test_IsFull()
{
    DrawLine();
    cout << "TEST: Test_IsFull" << endl;

    // Put tests here
    {   // Test 1
        cout << endl << "Test 1" << endl;
        List<int> testList;

        bool expectedValue = false;
        bool actualValue = testList.IsFull();

        cout << "Created list and did not add any items; IsFull() should be false." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
        testList.Clear();
    }

    {     // Test 2
        cout << endl << "Test 2" << endl;
        cout << "Prerequisites: PushBack()" << endl;
        List<int> testList;

        for (int i = 0; i < 100; i++)
        {
            testList.PushBack(2);
        }

        bool expectedValue = true;
        bool actualValue = testList.IsFull();

        cout << "Created list with 100 2's, IsFull() should be true." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
        testList.Clear();
    }

    {     // Test 3
        cout << endl << "Test 3" << endl;
        cout << "Prerequisites: PushBack()" << endl;
        List<int> testList;

        for (int i = 0; i < 101; i++)
        {
            testList.PushBack(2);
        }

        bool expectedValue = true;
        bool actualValue = testList.IsFull();

        cout << "Created list and attempted to add 101 2's, IsFull() should be true." << endl;
        if (expectedValue == actualValue)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
        testList.Clear();
    }
}

void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;

    // Put tests here
    {       // Test 1
        cout << endl << "Test 1" << endl;
        cout << "Prerequisites: PushBack()" << endl;
        List<string> testList;

        testList.PushFront("C");
        testList.PushFront("B");
        testList.PushFront("A");

        string expectedValues[3] = {
            "A",
            "B",
            "C"
        };

        string* actualValues[3] = {
            testList.Get(0),
            testList.Get(1),
            testList.Get(2)
        };

        cout << "Created list with A,B and C; Get(0), Get(1) and Get(2) should return A, B and C, respectively." << endl;
        if (expectedValues[0] == *actualValues[0] && expectedValues[1] == *actualValues[1] && expectedValues[2] == *actualValues[2])
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }
}

void Tester::Test_PushBack()
{
    DrawLine();
    cout << "TEST: Test_PushBack" << endl;

    // Put tests here
    {       // Test 1
  		cout << endl << "Test 1" << endl;

  		List<string> testList;

  		bool expectedValue = true;
  		bool actualValue = testList.PushBack("A");

  		cout << "Create a list, PushBack 1 item, should be successful" << endl;
  		if (expectedValue == actualValue)
  		{
  			cout << "Pass" << endl;
  		}
  		else
  		{
  			cout << "Fail" << endl;
  		}
  	}

  	{        // Test 2
  		cout << endl << "Test 2" << endl;

  		List<string> testList;
        for (int i = 0; i < 100; i++) {
            testList.PushBack("A");
        }

  		bool expectedValue = false;
  		bool actualValue = testList.PushBack("A");

  		cout << "Create a list with 100 items, PushBack 1 item, should be unsuccessful" << endl;
  		if (expectedValue == actualValue)
  		{
  			cout << "Pass" << endl;
  		}
  		else
  		{
  			cout << "Fail" << endl;
  		}
  	}
}

void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl;

    // Put tests here
    {     // Test 1
      cout << endl << "Test 1" << endl;
      List<int> testList;

      bool expectedValue = false;
      bool actualValue = testList.PopFront();

      cout << "Created empty list; PopFront() should be false." << endl;
      if (expectedValue == actualValue)
      {
        cout << "Pass" << endl;
      }
      else
      {
        cout << "Fail" << endl;
      }
    }

    {     // Test 2
      cout << endl << "Test 2" << endl;
      cout << "Prerequisites: PushBack()" << endl;
      List<string> testList;
      testList.PushBack("A");
      testList.PushBack("B");

      bool expectedValue = true;
      bool actualValue = testList.PopFront();

      cout << "Created list with two items; PopFront() should be true." << endl;
      if (expectedValue == actualValue)
      {
        cout << "Pass" << endl;
      }
      else
      {
        cout << "Fail" << endl;
      }
    }
}

void Tester::Test_PopBack()
{
    DrawLine();
    cout << "TEST: Test_PopBack" << endl;

    // Put tests here
    {
      // Test 1
      cout << endl << "Test 1" << endl;
      List<int> testList;

      bool expectedValue = false;
      bool actualValue = testList.PopBack();

      cout << "Created empty list; PopBack() should be false." << endl;
      if (expectedValue == actualValue)
      {
        cout << "Pass" << endl;
      }
      else
      {
        cout << "Fail" << endl;
      }
    }

    {
      // Test 2
      cout << endl << "Test 2" << endl;
      cout << "Prerequisites: PushBack()" << endl;
      List<int> testList;
      testList.PushBack(5);

      bool expectedValue = true;
      bool actualValue = testList.PopBack();

      cout << "Created list with 5; PopBack() should be true." << endl;
      if (expectedValue == actualValue)
      {
        cout << "Pass" << endl;
      }
      else
      {
        cout << "Fail" << endl;
      }
    }
}

void Tester::Test_Clear()       // Not really sure the best way to test Clear()
{
    DrawLine();
    cout << "TEST: Test_Clear" << endl;

    // Put tests here
    {     // Test 1
      cout << endl << "Test 1" << endl;
      List<int> testList;

      testList.Clear();

      bool expectedValue = true;
      bool actualValue = testList.IsEmpty();

      cout << "Created empty list; IsEmpty() should be true." << endl;
      if (expectedValue == actualValue)
      {
        cout << "Pass" << endl;
      }
      else
      {
        cout << "Fail" << endl;
      }
    }

    {     // Test 2
      cout << endl << "Test 2" << endl;
      cout << "Prerequisites: PushBack()" << endl;
      List<int> testList;
      testList.PushBack(3);
      testList.PushBack(5);
      testList.PushBack(7);

      testList.Clear();

      bool expectedValue = true;
      bool actualValue = testList.IsEmpty();

      cout << "Created list with A,B and C; Clear() should be true." << endl;
      if (expectedValue == actualValue)
      {
        cout << "Pass" << endl;
      }
      else
      {
        cout << "Fail" << endl;
      }
    }
}

void Tester::Test_Get()
{
    DrawLine();
    cout << "TEST: Test_Get" << endl;

    // Put tests here
    {     // Test 1
        cout << endl << "Test 1" << endl;
        cout << "Prerequisite: PushBack()" << endl;

        List<string> testlist;
        testlist.PushBack("A");
        testlist.PushBack("B");
        testlist.PushBack("C");


        string expectedValues[3] = {
            "A",
            "B",
            "C"
        };
        string* actualValues[3] = {
            testlist.Get(0),
            testlist.Get(1),
            testlist.Get(2)
        };

        // Leave the test before we de-reference the pointers
        // so that it doesn't crash.
        if (actualValues[0] == nullptr || actualValues[1] == nullptr || actualValues[2] == nullptr)
        {
            cout << "Got nullptrs; avoid segfault. Returning" << endl;
            return;
        }

        if (*actualValues[0] == expectedValues[0] &&
            *actualValues[1] == expectedValues[1] &&
            *actualValues[2] == expectedValues[2])
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }

    {     // Test 2
        cout << endl << "Test 2" << endl;
        cout << "Prerequisite: PushBack()" << endl;

        List<string> testlist;
        testlist.PushBack("A");
        testlist.PushBack("B");
        testlist.PushBack("C");

        string expectedValues[3] = {
            "A",
            "B",
            "C"
        };
        string* actualValues[4] = {
            testlist.Get(0),
            testlist.Get(1),
            testlist.Get(2),
            testlist.Get(3)
        };

        // Leave the test before we de-reference the pointers
        // so that it doesn't crash.
        if (actualValues[0] == nullptr || actualValues[1] == nullptr || actualValues[2] == nullptr)
        {
            cout << "Got nullptrs; avoid segfault. Returning" << endl;
            return;
        }

        if (*actualValues[0] == expectedValues[0] &&
            *actualValues[1] == expectedValues[1] &&
            *actualValues[2] == expectedValues[2] &&
            actualValues[3] == nullptr)
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }

    {
      // Test 3
      cout << endl << "Test 3" << endl;
      cout << "Prerequisites: PushBack()" << endl;
      List<string> testList;

      for (int i = 0; i < 100; i++)
      {
          testList.PushBack("A");
      }

      string* expectedValue = nullptr;
      string* actualValue = testList.Get(101);

      cout << "Created list with 100 A's; Get(101) should return nullptr." << endl;
      if (expectedValue == actualValue)
      {
        cout << "Pass" << endl;
      }
      else
      {
        cout << "Fail" << endl;
      }
    }
}

void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;

    // Put tests here
}

void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;

    // Put tests here
}

void Tester::Test_GetCountOf()
{
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl;

    // Put tests here
}

void Tester::Test_Contains()
{
    DrawLine();
    cout << "TEST: Test_Contains" << endl;

    // Put tests here
}

void Tester::Test_Remove()
{
    DrawLine();
    cout << "TEST: Test_Remove" << endl;

    // Put tests here
}

void Tester::Test_Insert()
{
    DrawLine();
    cout << "TEST: Test_Insert" << endl;

    // Put tests here
}
